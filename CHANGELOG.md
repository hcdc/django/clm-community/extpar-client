<!--
SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC0-1.0
-->

# Changelog

## v0.1.0: Initial release
