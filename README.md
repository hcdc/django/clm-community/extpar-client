<!--
SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
SPDX-FileCopyrightText: 2022-2024 ETH Zürich

SPDX-License-Identifier: CC-BY-4.0
-->

# Extpar Client

[![CI](https://codebase.helmholtz.cloud/hcdc/django/clm-community/extpar-client/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/hcdc/django/clm-community/extpar-client/-/pipelines?page=1&scope=all&ref=main)
[![Code coverage](https://codebase.helmholtz.cloud/hcdc/django/clm-community/extpar-client/badges/main/coverage.svg)](https://codebase.helmholtz.cloud/hcdc/django/clm-community/extpar-client/-/graphs/main/charts)
[![Latest Release](https://codebase.helmholtz.cloud/hcdc/django/clm-community/extpar-client/-/badges/release.svg)](https://codebase.helmholtz.cloud/hcdc/django/clm-community/extpar-client)
<!-- TODO: uncomment the following line when the package is published at https://pypi.org -->
<!-- [![PyPI version](https://img.shields.io/pypi/v/extpar-client.svg)](https://pypi.python.org/pypi/extpar-client/) -->
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![PEP8](https://img.shields.io/badge/code%20style-pep8-orange.svg)](https://www.python.org/dev/peps/pep-0008/)
[![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)
<!-- TODO: uncomment the following line when the package is registered at https://api.reuse.software -->
<!-- [![REUSE status](https://api.reuse.software/badge/codebase.helmholtz.cloud/hcdc/django/clm-community/extpar-client)](https://api.reuse.software/info/codebase.helmholtz.cloud/hcdc/django/clm-community/extpar-client) -->


Extpar interface to create external data set for a COSMO-CLM simulation area.

## Installation

Install this package in a dedicated python environment via

```bash
python -m venv venv
source venv/bin/activate
pip install extpar-client
```

To use this in a development setup, clone the [source code][source code] from
gitlab, start the development server and make your changes::

```bash
git clone https://codebase.helmholtz.cloud/hcdc/django/clm-community/extpar-client
cd extpar-client
python -m venv venv
source venv/bin/activate
make dev-install
```

More detailed installation instructions my be found in the [docs][docs].


[source code]: https://codebase.helmholtz.cloud/hcdc/django/clm-community/extpar-client
[docs]: https://hcdc.pages.hzdr.de/django/clm-community/extpar-clientinstallation.html
## Quick usage

The module `backend.py` defines the API to access the module. You can connect
it to the message broker at the CLM playground via

```bash
TOKEN=your-token
python -m extpar_client.backend \
    -t the-topic-that-you-choose \
    --websocket-url wss://hcdc.hereon.de/clm-playground/ws/dasf/ \
    --header "{\"authorization\": \"Token $TOKEN\"}" \
    listen
```

The client stub (i.e. the `api.py` module) can be generated with

```bash
python -m extpar_client.backend \
    -t the-topic-that-you-choose \
    --websocket-url wss://hcdc.hereon.de/clm-playground/ws/dasf/ \
    generate > extpar_client/api.py
```

To use it within a python script, first run

```bash
DE_BACKEND_HEADER="{\"authorization\": \"Token ${TOKEN}\"}"
```

in bash and then in python


```python
from extpar_client.api import ExtParClient
client = ExtParClient(...)
client.generate_and_download_files()
```

## Technical note

This package has been generated from the template
https://codebase.helmholtz.cloud/dasf/templates/dasf-backend-template.git.

See the template repository for instructions on how to update the skeleton for
this package.


## License information

Copyright © 2022-2024 Helmholtz-Zentrum hereon GmbH
Copyright © 2022-2024 ETH Zürich


Code files in this repository are licensed under the
EUPL-1.2, if not stated otherwise
in the file.

Documentation files in this repository are licensed under CC-BY-4.0, if not stated otherwise in the file.

Supplementary and configuration files in this repository are licensed
under CC0-1.0, if not stated otherwise
in the file.

Please check the header of the individual files for more detailed
information.



### License management

License management is handled with [``reuse``](https://reuse.readthedocs.io/).
If you have any questions on this, please have a look into the
[contributing guide][contributing] or contact the maintainers of
`extpar-client`.

[contributing]: https://hcdc.pages.hzdr.de/django/clm-community/extpar-clientcontributing.html
