# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""pytest configuration script for extpar-client."""
from __future__ import annotations

from typing import TYPE_CHECKING, Callable

import pytest

if TYPE_CHECKING:
    import subprocess as spr

    from dasf_broker.tests.live_ws_server_helper import ChannelsLiveServer


@pytest.fixture
def connected_module(
    db,
    monkeypatch,
    live_ws_server: ChannelsLiveServer,
    connect_module: Callable[[str, str], spr.Popen],
    random_topic: str,
) -> spr.Popen:
    """A process that connects the backend module to the message broker.

    This fixtures uses the ``connect_module`` fixture of the
    ``dasf-broker-django`` package to connect the backend module at
    ``extpar_client.backend`` to a live server that is
    started at localhost."""
    from extpar_client import api

    process = connect_module(random_topic, "extpar_client.backend")

    config = api.BackendModule.backend_config.messaging_config
    monkeypatch.setattr(config, "topic", random_topic)
    monkeypatch.setattr(
        config, "websocket_url", live_ws_server.ws_url + "/ws/"
    )

    return process
