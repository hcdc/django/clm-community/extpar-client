<!--
SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: EUPL-1.2
-->

# DASF message broker for local development

This django project can be used for local development.
