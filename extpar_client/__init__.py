# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Extpar Client

Extpar interface to create external data set for a COSMO-CLM simulation area.
"""

from __future__ import annotations

from . import _version

__version__ = _version.get_versions()["version"]

__author__ = "Philipp S. Sommer, Jonas Jucker"
__copyright__ = "2022-2024 Helmholtz-Zentrum hereon GmbH, ETH Zürich"
__credits__ = [
    "Philipp S. Sommer",
    "Jonas Jucker",
]
__license__ = "EUPL-1.2"

__maintainer__ = "Philipp S. Sommer"
__email__ = "philipp.sommer@hereon.de, jonas.jucker@c2sm.ethz.ch"

__status__ = "Development"
