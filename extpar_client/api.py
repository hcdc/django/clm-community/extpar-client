"""
Create external datasets for a COSMO-CLM simulation area.
"""

# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from typing import Any, Callable, Dict, List, Literal, Tuple

from demessaging import BackendModule as _BackendModule
from demessaging import main, registry
from demessaging.config import ModuleConfig
from pydantic import BaseModel

NoneType = type(None)


__all__ = ["ExtParClient", "version_info"]


@registry.register_type
class PolygonData(BaseModel):
    """Data to draw a polygon."""

    attrs: Dict[str, float]
    rings: List[Tuple[float, float]]


@registry.register_type
class ExtparOutput(PolygonData):
    """Data for generating the extpar output."""

    url: str
    version_info: Dict[str, str]


class ExtParClient:
    """
    A client to generate external data sets for COSMO-CLM simulations.
    """

    def __init__(
        self,
        origin_lon: float,
        origin_lat: float,
        dlon: float,
        dlat: float,
        nlon: int,
        nlat: int,
        startlon: float,
        startlat: float,
        oro: Literal["GLOBE", "ASTER", "MERIT"] = "GLOBE",
        landuse: Literal["GLOBCOVER", "GLC2000", "ECOCLIMAP"] = "GLOBCOVER",
        soil: Literal["FAO-DSMW"] = "FAO-DSMW",
        aot: Literal["NASA/GISS", "AeroCom1, MPI_MET"] = "NASA/GISS",
        albedo: Literal["MODIS dry & sat", "MODIS12 vis"] = "MODIS dry & sat",
        orofilter: bool = False,
        sgsl: bool = False,
        urban: bool = False,
    ):
        """
        Parameters
        ----------
        origin_lon : float
            The geographical longitude of the origin (0,0) of the rotated
            system.
        origin_lat : float
            The geographical latitude of the origin (0,0) of the rotated
            system.
        dlon : float
            The grid mesh size in degrees in the rotated coordinate system in
            x-direction.
        dlat : float
            The grid mesh size in degrees in the rotated coordinate system in
            y-direction.
        nlon : int
            The total number of grid points in X-direction. Maximum number is
            3000.
        nlat : int
            The total number of grid points in Y-direction. Maximum number is
            3000.
        startlon : float
            The longitude of the lower left corner of the domain in rotated
            coordinates. The value of STARTLON is counted by starting with zero
            at origin lon.
        startlat : float
            The latitude of the lower left corner of the domain in rotated
            coordinates. The value of STARTLAT is counted by starting with zero
            at origin lat.
        oro : Literal[GLOBE, ASTER, MERIT], optional
            The preferred orography data set.

            - GLOBE GLOBE orography
            - ASTER ASTER orography. Valid for the region 60S to 60N. Grid
              width is restricted to <0.05 degrees.
        landuse : Literal[GLOBCOVER, GLC2000, ECOCLIMAP], optional
            The preferred land use data set.

            - GLC2000 land use
            - GLOBCOVER land use
            - ECOCLIMAP land use
        soil : Literal[FAO, optional
             Soil characteristics data set

            - FAO-DSMW FAO-DSMW data set
            - HWSD TERRA HWSD data TERRA mapping 1D
        aot : Literal[NASA, optional
            The preferred data set for the aerosol climatology.

            - NASA/GISS NASA/GISS data set
            - AeroCom1, MPI_MET AeroCom1, MPI_MET data set
        albedo : Literal[MODIS dry, optional
            The preferred data set for use as the solar surface albedo.

            - MODIS dry & sat only dry and saturated soil albedo (itype_albedo=2)
            - MODIS12 vis MODIS monthly albedo, visible part (itype_albedo=3) !! only available for versions COSMO-CLM_clm3 and newer !!
        orofilter : bool, optional
             Enable filtering the orography here or later in INT2LM.

            - No:  No Filtering applied (default)
            - Yes: Filtering applied. In this case the pre-settings are as follows:

              ilow_pass_oro=4,
              numfilt_oro=1,
              ilow_pass_xso=5,
              lxso_first=.FALSE.,
              numfilt_xso=1,
              rxso_mask=750.0,
              eps_filter=0.1,
              rfill_valley=0.0,
              ifill_valley=1
        sgsl : bool, optional
            Calculate subgrid-scale parameter S_ORO.

            - No: No calculating of S_ORO (default)
            - Yes: S_ORO will be calculated (this option is presently not stable, for certain domains/resolutions etc. it may crash)
        urban : bool, optional
            Include surface properties for use in TERRA_URB.
            Note: There is not yet a check for valid lon/lat boundaries!!

            - No: No urban parameters. Choose this for polar regions.
            - Yes: fine resolution background data for ISA, valid for mostly
              Europe 60W-60E, 75N-12N
        """
        self._request_base: Dict[str, Any] = {
            "class_name": "ExtParClient",
            "origin_lon": origin_lon,
            "origin_lat": origin_lat,
            "dlon": dlon,
            "dlat": dlat,
            "nlon": nlon,
            "nlat": nlat,
            "startlon": startlon,
            "startlat": startlat,
            "oro": oro,
            "landuse": landuse,
            "soil": soil,
            "aot": aot,
            "albedo": albedo,
            "orofilter": orofilter,
            "sgsl": sgsl,
            "urban": urban,
        }

    def generate_and_upload_files(self) -> ExtparOutput:
        """
        Generate the data and get a download link.

        This method generates the external data sets for the provided input
        parameters and generates a download link.
        """
        request = self._request_base.copy()
        request["function"] = {
            "func_name": "generate_and_upload_files",
        }

        model = BackendModule.model_validate(request)
        response = model.compute()

        return response.root  # type: ignore

    def generate_preview_polygon(self) -> PolygonData:
        """
        Generate a polygon for a preview
        """
        request = self._request_base.copy()
        request["function"] = {
            "func_name": "generate_preview_polygon",
        }

        model = BackendModule.model_validate(request)
        response = model.compute()

        return response.root  # type: ignore


def version_info() -> Dict[str, str]:
    """
    Get the version of extpar and extpar_client.
    """
    request = {
        "func_name": "version_info",
    }

    model = BackendModule.model_validate(request)
    response = model.compute()

    return response.root  # type: ignore


backend_config = ModuleConfig.model_validate_json(
    """
{
    "messaging_config": {
        "topic": "webpep",
        "max_workers": null,
        "queue_size": null,
        "max_payload_size": 512000,
        "producer_keep_alive": 120,
        "producer_connection_timeout": 30,
        "websocket_url": "https://www.clm-community.eu/ws/dasf",
        "producer_url": "",
        "consumer_url": ""
}
}
"""
)

_creator: Callable
if __name__ == "__main__":
    _creator = main
else:
    _creator = _BackendModule.create_model

BackendModule = _creator(
    __name__,
    config=backend_config,
    class_name="BackendModule",
    members=[ExtParClient, version_info],
)
