"""Create external datasets for a COSMO-CLM simulation area.
"""

# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
# SPDX-FileCopyrightText: 2022-2024 ETH Zürich
#
# SPDX-License-Identifier: EUPL-1.2

import datetime as dt
import os
import os.path as osp
from textwrap import dedent
from typing import Dict, List, Literal, Tuple
from uuid import uuid4

import numpy as np
from demessaging import configure, main, registry
from pydantic import BaseModel
from pydantic_settings import BaseSettings, SettingsConfigDict

__all__ = ["ExtParClient", "version_info"]


aerosols = {"NASA/GISS": 1, "AeroCom1, MPI_MET": 2}
luse = {"GLOBCOVER": 1, "GLC2000": 2, "ECOCLIMAP": 4}
soil_type = {"FAO-DSMW": 1}
alb = {"MODIS dry & sat": 2, "MODIS12 vis": 3}  # Missing type 1
orography = {"GLOBE": 1, "ASTER": 2, "MERIT": 3}


class ExtParClientSettings(BaseSettings):
    """Base class for messaging configs."""

    model_config = SettingsConfigDict(env_prefix="extpar_backend_")

    raw_data_path: str
    account: str
    host: str = "levante"
    run_dir_base: str = "."

    dry_run: bool = False


class SwiftSettings(BaseSettings):
    """Settings for the swiftbrowser."""

    model_config = SettingsConfigDict(env_file=osp.expanduser("~/swift.env"))

    os_auth_token: str
    os_storage_url: str

    swift_url: str = "https://swift.dkrz.de"
    swift_container: str = "extpar_client"


@registry.register_type
class PolygonData(BaseModel):
    """Data to draw a polygon."""

    attrs: Dict[str, float]
    rings: List[Tuple[float, float]]


@registry.register_type
class ExtparOutput(PolygonData):
    """Data for generating the extpar output."""

    url: str
    version_info: Dict[str, str]


def version_info() -> Dict[str, str]:
    """Get the version of extpar and extpar_client."""
    import extpar_client

    info = {
        "extpar-client": extpar_client.__version__,
    }
    try:
        import extpar
    except ModuleNotFoundError:
        pass
    else:
        info["extpar"] = extpar.__version__
    return info


@configure(methods=["generate_and_upload_files", "generate_preview_polygon"])
class ExtParClient:
    """A client to generate external data sets for COSMO-CLM simulations."""

    def __init__(
        self,
        origin_lon: float,
        origin_lat: float,
        dlon: float,
        dlat: float,
        nlon: int,
        nlat: int,
        startlon: float,
        startlat: float,
        oro: Literal["GLOBE", "ASTER", "MERIT"] = "GLOBE",
        landuse: Literal["GLOBCOVER", "GLC2000", "ECOCLIMAP"] = "GLOBCOVER",
        soil: Literal["FAO-DSMW"] = "FAO-DSMW",
        aot: Literal["NASA/GISS", "AeroCom1, MPI_MET"] = "NASA/GISS",
        albedo: Literal["MODIS dry & sat", "MODIS12 vis"] = "MODIS dry & sat",
        orofilter: bool = False,
        sgsl: bool = False,
        urban: bool = False,
    ):
        """
        Parameters
        ----------
        origin_lon : float
            The geographical longitude of the origin (0,0) of the rotated
            system.
        origin_lat : float
            The geographical latitude of the origin (0,0) of the rotated
            system.
        dlon : float
            The grid mesh size in degrees in the rotated coordinate system in
            x-direction.
        dlat : float
            The grid mesh size in degrees in the rotated coordinate system in
            y-direction.
        nlon : int
            The total number of grid points in X-direction. Maximum number is
            3000.
        nlat : int
            The total number of grid points in Y-direction. Maximum number is
            3000.
        startlon : float
            The longitude of the lower left corner of the domain in rotated
            coordinates. The value of STARTLON is counted by starting with zero
            at origin lon.
        startlat : float
            The latitude of the lower left corner of the domain in rotated
            coordinates. The value of STARTLAT is counted by starting with zero
            at origin lat.
        oro : Literal[GLOBE, ASTER, MERIT], optional
            The preferred orography data set.

            - GLOBE GLOBE orography
            - ASTER ASTER orography. Valid for the region 60S to 60N. Grid
              width is restricted to <0.05 degrees.
        landuse : Literal[GLOBCOVER, GLC2000, ECOCLIMAP], optional
            The preferred land use data set.

            - GLC2000 land use
            - GLOBCOVER land use
            - ECOCLIMAP land use
        soil : Literal[FAO, optional
             Soil characteristics data set

            - FAO-DSMW FAO-DSMW data set
            - HWSD TERRA HWSD data TERRA mapping 1D
        aot : Literal[NASA, optional
            The preferred data set for the aerosol climatology.

            - NASA/GISS NASA/GISS data set
            - AeroCom1, MPI_MET AeroCom1, MPI_MET data set
        albedo : Literal[MODIS dry, optional
            The preferred data set for use as the solar surface albedo.

            - MODIS dry & sat only dry and saturated soil albedo (itype_albedo=2)
            - MODIS12 vis MODIS monthly albedo, visible part (itype_albedo=3) !! only available for versions COSMO-CLM_clm3 and newer !!
        orofilter : bool, optional
             Enable filtering the orography here or later in INT2LM.

            - No:  No Filtering applied (default)
            - Yes: Filtering applied. In this case the pre-settings are as follows:

              ilow_pass_oro=4,
              numfilt_oro=1,
              ilow_pass_xso=5,
              lxso_first=.FALSE.,
              numfilt_xso=1,
              rxso_mask=750.0,
              eps_filter=0.1,
              rfill_valley=0.0,
              ifill_valley=1
        sgsl : bool, optional
            Calculate subgrid-scale parameter S_ORO.

            - No: No calculating of S_ORO (default)
            - Yes: S_ORO will be calculated (this option is presently not stable, for certain domains/resolutions etc. it may crash)
        urban : bool, optional
            Include surface properties for use in TERRA_URB.
            Note: There is not yet a check for valid lon/lat boundaries!!

            - No: No urban parameters. Choose this for polar regions.
            - Yes: fine resolution background data for ISA, valid for mostly
              Europe 60W-60E, 75N-12N
        """

        self._nml_params = dict(
            origin_lon=origin_lon,
            origin_lat=origin_lat,
            nlon=nlon,
            nlat=nlat,
            startlon=startlon,
            startlat=startlat,
            dlon=dlon,
            dlat=dlat,
        )
        self._nml_params["pollon"] = self.pollon
        self._nml_params["pollat"] = self.pollat
        self._nml_params["polgam"] = self.polgam

        self._webpep_params = dict(
            lfilter_oro=orofilter,
            lsgsl=sgsl,
            lurban=urban,
            ilu_type=luse[landuse],
            ialb_type=alb[albedo],
            isoil_type=soil_type[soil],
            itopo_type=orography[oro],
            iaot_type=aerosols[aot],
        )

        self.settings = settings = ExtParClientSettings()
        if os.getenv("SWIFT_ENV_FILE"):
            self.swift_settings = SwiftSettings(
                _env_file=os.environ["SWIFT_ENV_FILE"]
            )
        else:
            self.swift_settings = SwiftSettings()

        self.uuid = str(uuid4())

        self.id = "extpar_client_" + self.uuid

        self.run_dir = osp.join(settings.run_dir_base, self.id)

    def run_extpar(self):
        """Create the files"""
        from extpar.WrapExtpar import generate_external_parameters

        nml = (
            """
            &lmgrid
            pollon = %(pollon)s
            pollat = %(pollat)s
            polgam = %(polgam)s
            dlon = %(dlon)s
            dlat = %(dlat)s
            ie_tot = %(nlon)s
            je_tot = %(nlat)s
            startlon_tot = %(startlon)s
            startlat_tot = %(startlat)s
            /
            """
            % self._nml_params
        )

        os.makedirs(self.run_dir)

        nml_file = osp.join(self.run_dir, "INPUT_COSMO_GRID_" + self.id)
        with open(nml_file, "w") as f:
            f.write(dedent(nml))

        generate_external_parameters(
            input_cosmo_grid=nml_file,
            raw_data_path=self.settings.raw_data_path,
            run_dir=self.run_dir,
            account=self.settings.account,
            host=self.settings.host,
            **self._webpep_params,
        )

    def upload_files(self, timestamp: str) -> str:
        """Upload the files and generate a download link."""
        from swiftclient.client import Connection
        from swiftclient.utils import generate_temp_url

        settings = self.swift_settings

        c = Connection()

        c.url = settings.os_storage_url
        c.token = settings.os_auth_token

        ofile = f"external_parameter_{timestamp}_{self.uuid}.nc"

        with open(osp.join(self.run_dir, "external_parameter.nc"), "rb") as f:
            c.put_object(settings.swift_container, ofile, f)

        # now create a new temporary url key
        key = str(uuid4())
        params, http_con = c.http_connection()
        http_con.request(
            "POST",
            params.path,
            headers={
                "X-Auth-Token": c.token,
                "X-Account-Meta-Temp-URL-Key": key,
            },
        )

        # generate a temporary url that is valid for 7
        swift_account = c.url.split("/")[-1]
        temp_url = generate_temp_url(
            f"/v1/{swift_account}/{settings.swift_container}/{ofile}",
            86400 * 7,  # seven days
            key,
            "GET",
        )
        return settings.swift_url + temp_url

    def generate_and_upload_files(self) -> ExtparOutput:
        """Generate the data and get a download link.

        This method generates the external data sets for the provided input
        parameters and generates a download link.
        """
        timestamp = dt.datetime.now().strftime("%Y%m%d-%H%M%S")
        if self.settings.dry_run:
            return ExtparOutput(
                url="https://www.clm-community.eu",
                **self.generate_preview_polygon().dict(),
            )
        self.run_extpar()
        download_link = self.upload_files(timestamp)
        return ExtparOutput(
            url=download_link,
            version_info=version_info(),
            **self.generate_preview_polygon().dict(),
        )

    @property
    def pollat(self):
        rlon = 0.0
        rlat = 90.0
        origin_lon = self._nml_params["origin_lon"]
        origin_lat = self._nml_params["origin_lat"]
        if origin_lon == 0.0 and origin_lat == 0.0:
            return rlat
        sin_origin_lat = np.sin(np.deg2rad(origin_lat))
        cos_origin_lat = np.cos(np.deg2rad(origin_lat))
        sin_rlon = np.sin(np.deg2rad(rlon))
        cos_rlon = np.cos(np.deg2rad(rlon))
        sin_rlat = np.sin(np.deg2rad(rlat))
        cos_rlat = np.cos(np.deg2rad(rlat))
        x = cos_origin_lat * cos_rlat * cos_rlon - sin_origin_lat * sin_rlat
        y = cos_rlat * sin_rlon
        z = sin_origin_lat * cos_rlat * cos_rlon + cos_origin_lat * sin_rlat
        glat = 0
        if x * x + y * y == 0.0:
            if z > 0.0:
                glat = 90.0
            if z < 0.0:
                glat = -90.0
        else:
            glat = np.rad2deg(np.arctan(z / np.sqrt(x * x + y * y)))
        return glat

    @property
    def pollon(self):
        rlon = 0.0
        rlat = 90.0
        origin_lon = self._nml_params["origin_lon"]
        origin_lat = self._nml_params["origin_lat"]
        if origin_lon == 0 and origin_lat == 0:
            return rlon
        sin_origin_lat = np.sin(np.deg2rad(origin_lat))
        cos_origin_lat = np.cos(np.deg2rad(origin_lat))
        sin_rlon = np.sin(np.deg2rad(rlon))
        cos_rlon = np.cos(np.deg2rad(rlon))
        sin_rlat = np.sin(np.deg2rad(rlat))
        cos_rlat = np.cos(np.deg2rad(rlat))
        x = cos_origin_lat * cos_rlat * cos_rlon - sin_origin_lat * sin_rlat
        y = cos_rlat * sin_rlon

        if abs(origin_lat) == 90:
            glon = 0.0
        elif x == 0:
            glon = 0.0
            if y > 0.0:
                glon = 90.0
            if y < 0:
                glon = 270
        elif x > 0.0:
            glon = np.rad2deg(np.arctan(y / x))
        else:
            glon = 180 + np.rad2deg(np.arctan(y / x))
        glon = glon + origin_lon
        if glon >= 360.0:
            glon = glon - 360.0
        if glon > 180.0:
            glon = glon - 360
        return glon

    @property
    def polgam(self):
        origin_lat = self._nml_params["origin_lat"]
        if origin_lat <= 0.0:
            return 180.0
        else:
            return 0.0

    @property
    def preview_polygon(self):
        import cartopy.crs as ccrs

        crs = ccrs.RotatedPole(self.pollon, self.pollat, self.polgam)
        target = ccrs.PlateCarree()
        nlon = self._nml_params["nlon"]
        nlat = self._nml_params["nlat"]
        dlon = self._nml_params["dlon"]
        dlat = self._nml_params["dlat"]
        startlon = self._nml_params["startlon"]
        startlat = self._nml_params["startlat"]
        x = np.linspace(startlon, startlon + nlon * dlon, nlon)
        y = np.linspace(startlat, startlat + nlat * dlat, nlat)

        left = np.r_[[[startlon] * y.size], [y]].T

        top = np.r_[[x], [[startlat + nlat * dlat] * x.size]].T

        right = np.r_[[[startlon + nlon * dlon] * y.size], [y[::-1]]].T

        bottom = np.r_[[x[::-1]], [[startlat] * x.size]].T

        rings = np.r_[left, top, right, bottom]

        transformed = target.transform_points(crs, rings[:, 0], rings[:, 1])[
            :, :2
        ]

        # make sure the transformed points are monotonically increasing in
        # x-direction
        is_increasing = transformed[1, 0] > transformed[0, 0]
        if is_increasing:
            breakpoints = np.where(
                transformed[1:, 0] - transformed[:-1, 0] < 0
            )[0]
        else:
            breakpoints = np.where(
                transformed[1:, 0] - transformed[:-1, 0] > 0
            )[0]
        if len(breakpoints):
            idx = breakpoints[0]
            transformed = np.roll(transformed, len(transformed) - idx - 1, 0)

        # if the pole is contained in the polygon, we should add it manually
        pole_lat = -90 if self._nml_params["origin_lat"] <= 0.0 else 90
        pol_x, pol_y = crs.transform_point(180, pole_lat, target)
        if pol_y >= y.min() and pol_y <= y.max():
            transformed = np.r_[
                [[transformed[0, 0], pole_lat]],
                transformed,
                [[transformed[-1, 0], pole_lat]],
            ]
        elif pol_x >= x.min() and pol_x <= x.max():
            transformed[transformed[:, 0] < 0, 0] += 360

        return transformed[:, :2].tolist()

    def generate_preview_polygon(self) -> PolygonData:
        """Generate a polygon for a preview"""
        attrs = self._nml_params.copy()
        attrs.update(
            pollon=self.pollon, pollat=self.pollat, polgam=self.polgam
        )
        return PolygonData(attrs=attrs, rings=self.preview_polygon)


if __name__ == "__main__":
    if os.getenv("WEBPEP_MESSAGING_ENV_FILE"):
        from demessaging.config import ModuleConfig, WebsocketURLConfig

        config = ModuleConfig(
            messaging_config=WebsocketURLConfig(
                topic="webpep",
                _env_file=os.environ["WEBPEP_MESSAGING_ENV_FILE"],
            )
        )
        main(config=config)
    else:
        main(messaging_config=dict(topic="webpep"))
