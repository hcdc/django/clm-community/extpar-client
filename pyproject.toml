# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

[build-system]
build-backend = 'setuptools.build_meta'
requires = ['setuptools >= 61.0', 'versioneer[toml]']

[project]
name = "extpar-client"
dynamic = ["version"]
description = "Extpar interface to create external data set for a COSMO-CLM simulation area."

readme = "README.md"
keywords = [
    "dasf",
    "clm-community",
    "cosmo",
    "cosmo-clm",
    "extpar",
    "webpep",
    "datahub",
    "distributed data analysis",
]

authors = [
    { name = 'Philipp S. Sommer', email = 'philipp.sommer@hereon.de' },
    { name = 'Jonas Jucker', email = 'jonas.jucker@c2sm.ethz.ch' },
]
maintainers = [
    { name = 'Philipp S. Sommer', email = 'philipp.sommer@hereon.de' },
]
license = { text = 'EUPL-1.2' }

classifiers = [
    "Intended Audience :: Developers",
    "License :: OSI Approved :: European Union Public Licence 1.2 (EUPL 1.2)",
    "Operating System :: OS Independent",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3 :: Only",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Typing :: Typed",
]

requires-python = '>= 3.9'
dependencies = [
    "demessaging>=0.4.0",
    # add your dependencies here
]

[project.urls]
Homepage = 'https://codebase.helmholtz.cloud/hcdc/django/clm-community/extpar-client'
Documentation = "https://hcdc.pages.hzdr.de/django/clm-community/extpar-client"
Source = "https://codebase.helmholtz.cloud/hcdc/django/clm-community/extpar-client"
Tracker = "https://codebase.helmholtz.cloud/hcdc/django/clm-community/extpar-client/issues/"


[project.optional-dependencies]

backend = [
  "demessaging[backend]",
  "python-dotenv",
  "cartopy",
  "python-swiftclient",
]

backend-full = [
  "extpar-client[backend]",
  "extpar>=5.13,<5.14",
]

testsite = [
    "extpar-client[backend]",
    "tox",
    "isort==5.12.0",
    "black==23.1.0",
    "blackdoc==0.3.8",
    "flake8==6.0.0",
    "pre-commit",
    "mypy",
    "dasf-broker-django",
    "djangorestframework",
    "pytest-django",
    "pytest-cov",
    "reuse",
    "cffconvert",

]
docs = [
    "extpar-client[backend]",
    "autodocsumm",
    "sphinx-rtd-theme",
    "hereon-netcdf-sphinxext",
    "sphinx-design",
    "myst_parser",
]
dev = [
    "extpar-client[testsite]",
    "extpar-client[docs]",
    "PyYAML",
    "types-PyYAML",
]


[tool.mypy]
ignore_missing_imports = true
plugins = [
    "pydantic.mypy",
]

[tool.setuptools]
zip-safe = false
license-files = ["LICENSES/*"]

[tool.setuptools.package-data]
extpar_client = ["py.typed"]

[tool.setuptools.packages.find]
namespaces = false
exclude = [
    'docs',
    'tests*',
    'examples'
]

[tool.pytest.ini_options]
addopts = '-v'
DJANGO_SETTINGS_MODULE = "testproject.settings"

[tool.versioneer]
VCS = 'git'
style = 'pep440'
versionfile_source = 'extpar_client/_version.py'
versionfile_build = 'extpar_client/_version.py'
tag_prefix = 'v'
parentdir_prefix = 'extpar-client-'

[tool.isort]
profile = "black"
line_length = 79
src_paths = ["extpar_client"]
float_to_top = true
known_first_party = "extpar_client"

[tool.black]
line-length = 79
target-version = ['py39']

[tool.coverage.run]
omit = ["extpar_client/_version.py"]
