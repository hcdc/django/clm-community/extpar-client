# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Test module for :mod:`extpar_client.api`."""
from typing import Any

from extpar_client import api


def test_show_version(connected_module: Any, random_topic: str):
    from extpar_client import __version__ as ref_version

    version_info = api.version_info()
    assert "extpar-client" in version_info
    assert version_info["extpar-client"] == ref_version
