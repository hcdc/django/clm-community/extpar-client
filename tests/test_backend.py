# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Test module for :mod:`extpar_client.backend`."""


def test_show_version():
    from extpar_client import __version__ as ref_version
    from extpar_client import backend

    version_info = backend.version_info()
    assert "extpar-client" in version_info
    assert version_info["extpar-client"] == ref_version
