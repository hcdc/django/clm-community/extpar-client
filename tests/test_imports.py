# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Test file for imports."""


def test_package_import():
    """Test the import of the main package."""
    import extpar_client  # noqa: F401


def test_backend_import():
    import extpar_client.backend  # noqa: F401


def test_api_import():
    import extpar_client.api  # noqa: F401
